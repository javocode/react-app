import React from 'react';

export default class CreateTodo extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			error: null
		};
	}

	renderError() {
		if (!this.state.error) { return null;}

		return <div style={{color: 'red'}}>{this.state.error}</div>;
	}

	render() {
		return (
			<form onSubmit={this.handleCreate.bind(this)}>
				<input type="text" placeholder="what do i need to do?" ref="createInput"/>
				<button>Create</button>
				{this.renderError()}
			</form>
		);
	}

	handleCreate(event) {
		event.preventDefault();

		const createInput = this.refs.createInput;
		const task = createInput.value;
		const validateinput = this.validateinput(task);
		if (validateinput) {
			this.setState({error: validateinput});
			return;
		}
		this.setState({error: null});
		this.props.createTask(task);
		this.refs.createInput.value = '';
	}

	validateinput(task) {
		if (!task) {
			return "Please enter a task";
		} else if (_.find(this.props.todos, todo => todo.task === task)) {
			return "Task already exists";
		} else {
			return null;
		}
	}
}